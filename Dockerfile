ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as builder

ENV PHP_INI_PATH=/usr/local/etc/php

COPY libgd.tar.gz oniguruma.tar.gz /
COPY php.tar.gz /usr/local/src/
COPY scripts/docker-php* /usr/local/bin/
COPY *.rpm signatures/RPM-GPG-KEY-CentOS-Official signatures/RPM-GPG-KEY-EPEL-8 /

# Install build dependencies
RUN dnf upgrade -y && \
    dnf install -y --nodocs \
      autoconf \
      automake \
      bzip2-devel \
      diffutils \
      file \
      fontconfig-devel \
      freetype-devel \
      make \
      libtool \
      gcc \
      gcc-c++ \
      ghostscript \
      glib2-devel \
      libcurl-devel \
      libjpeg-devel \
      libpng-devel \
      librsvg2 \
      libtiff-devel \
      libX11-devel \
      libxml2-devel \
      libzip-devel \
      libwebp-devel \
      openldap-devel \
      openldap \
      openssl-devel \
      postgresql-devel \
      sqlite-devel \
      zlib-devel \
      openldap-devel && \
    dnf clean all

# Install Dependencies
RUN rpm --import RPM-GPG-KEY-CentOS-Official && \
    rpm --import RPM-GPG-KEY-EPEL-8 && \
    dnf install --setopt=tsflags=nodocs -y \
      re2c.rpm \
      bison.rpm \
      bison-devel.rpm \
      libsodium.rpm \
      libsodium-devel.rpm \
      readline-devel.rpm \
      libargon2.rpm \
      libargon2-devel.rpm

# Build libgd and oniguruma
RUN mkdir -p /usr/local/src/{libgd,oniguruma} && \
    tar -zxf libgd.tar.gz --strip-components=1 -C /usr/local/src/libgd && \
    tar -zxf oniguruma.tar.gz --strip-components=1 -C /usr/local/src/oniguruma && \
    cd /usr/local/src/libgd && \
    ./configure --libdir=/usr/lib64 && \
    make install && \
    cd /usr/local/src/oniguruma && \
    ./configure --libdir=/usr/lib64 && \
    make install

# Build PHP
RUN mkdir -p /usr/local/src/php \
    && tar -C /usr/local/src/php --strip-components=1 -zxf /usr/local/src/php.tar.gz \
    && cd /usr/local/src/php \
    && ./buildconf --force \
    && ./configure \
        --with-fpm-user=1001 \
        --with-fpm-group=1001 \
        --with-config-file-path=${PHP_INI_PATH} \
        --with-config-file-scan-dir=${PHP_INI_PATH}/conf.d \
        --enable-option-checking=fatal \
        --with-libdir=lib64 \
        --without-pear \
        --with-mhash \
        --with-pic \
        --with-curl \
        --with-iconv \
        --with-openssl \
        --with-readline \
        --with-zlib \
        --with-ldap \
        --with-mysqli \
        --with-pdo-sqlite=/usr \
        --with-pdo-mysql=mysqlnd \
        --with-pdo-pgsql \
        --with-sqlite3=/usr \
        --enable-mbstring \
        --enable-mysqlnd \
        --enable-ftp \
        --enable-gd \
        --enable-opcache \
        --with-password-argon2 \
        --with-sodium=shared \
        --with-jpeg \
        --with-webp \
        --with-zip \
        --with-freetype \
        --disable-phpdbg \
        --disable-cgi \
        --enable-fpm \
    && make -j4 \
    && make install \
    && for cert in $(find /usr/local/src/php/ext/ -name "*.pem" -o -name "*.phpt" -o -name "*.crt" -o -name "*.key"); do rm $cert; done \
    && sed 's!=NONE/!=!g' /usr/local/etc/php-fpm.conf.default | tee /usr/local/etc/php-fpm.conf > /dev/null \
    && mv /usr/local/etc/php-fpm.d/www.conf.default /usr/local/etc/php-fpm.d/www.conf

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

COPY *.rpm signatures/RPM-GPG-KEY-CentOS-Official signatures/RPM-GPG-KEY-EPEL-8 /

# persistent / runtime deps
RUN groupadd -g 1001 php-fpm && \
    useradd -r -u 1001 -g php-fpm php-fpm && \
    dnf upgrade -y && \
    rpm --import RPM-GPG-KEY-CentOS-Official && \
    rpm --import RPM-GPG-KEY-EPEL-8 && \
    dnf install --setopt=tsflags=nodocs -y \
      ca-certificates \
      curl \
      xz-libs && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    rm *.rpm

ENV PHP_INI_PATH=/usr/local/etc/php

RUN mkdir -p ${PHP_INI_PATH}/conf.d /usr/local/etc/php-fpm.d /usr/local/var/log/php-fpm \
    && chown -R 1001:1001 /usr/local/var/log/php-fpm/ \
    && { \
    echo '[global]'; \
    echo 'error_log = /proc/self/fd/2'; \
    echo; echo '; https://github.com/docker-library/php/pull/725#issuecomment-443540114'; echo 'log_limit = 8192'; \
    echo; \
    echo '[www]'; \
    echo '; if we send this to /proc/self/fd/1, it never appears'; \
    echo 'access.log = /proc/self/fd/2'; \
    echo; \
    echo 'clear_env = no'; \
    echo; \
    echo '; Ensure worker stdout and stderr are sent to the main error log.'; \
    echo 'catch_workers_output = yes'; \
    echo 'decorate_workers_output = no'; \
    } | tee /usr/local/etc/php-fpm.d/docker.conf \
    && { \
    echo '[global]'; \
    echo 'daemonize = no'; \
    echo; \
    echo '[www]'; \
    echo 'listen = 9000'; \
    } | tee /usr/local/etc/php-fpm.d/zz-docker.conf \
    && mkdir -p /var/www/html/ \
    && chown 1001:1001 /var/www/html \
    && chmod 755 /var/www/html \
    && chown -R root:root /usr/local/etc/

COPY --from=builder --chown=root:root /usr/local/sbin/php-fpm /usr/local/bin/php /usr/local/bin/phar /usr/local/bin/php-config /usr/local/bin/phpize /usr/local/bin/
COPY --from=builder --chown=root:root /usr/local/php /usr/local/php
COPY --from=builder --chown=root:root /usr/local/etc/php-fpm.conf /usr/local/etc/
COPY --from=builder --chown=root:root /usr/local/etc/php-fpm.d/ /usr/local/etc/php-fpm.d/
COPY --from=builder --chown=root:root /usr/local/include/php /usr/local/include/php
COPY --from=builder --chown=root:root /usr/local/lib/ /usr/local/lib/
COPY --from=builder --chown=root:root /usr/lib64/ /usr/lib64/
COPY config/php.ini ${PHP_INI_PATH}/php.ini
COPY scripts/docker-php-entrypoint /usr/local/bin/entrypoint

# CCE fixes
COPY scripts/oscap-fixes/ /oscap-fixes/

RUN /oscap-fixes/xccdf_org.ssgproject.content_rule_file_groupownership_system_commands_dirs.sh && \
    /oscap-fixes/xccdf_org.ssgproject.content_rule_file_ownership_binary_dirs.sh && \
    /oscap-fixes/xccdf_org.ssgproject.content_rule_file_permissions_binary_dirs.sh && \
    df --local -P | awk '{if (NR!=1) print $6}' \
    | xargs -I '{}' find '{}' -xdev -type d \
    \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null \
    | xargs -I '{}' chmod a+t '{}'

RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=60'; \
        echo 'opcache.fast_shutdown=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

USER 1001

WORKDIR "/var/www/html"

# Override stop signal to stop process gracefully
# https://github.com/php/php-src/blob/17baa87faddc2550def3ae7314236826bc1b1398/sapi/fpm/php-fpm.8.in#L163
STOPSIGNAL SIGQUIT

EXPOSE 9000

ENTRYPOINT ["entrypoint"]
CMD ["php-fpm"]

HEALTHCHECK --interval=5s --timeout=1s \
    CMD curl -s http://localhost:9000; if [ $? = 56 ]; then exit 0; else exit 1; fi
